# Foo

View me at [foo.instant-project.johanvo.me](foo.instant-project.johanvo.me) 


## Requirements
For development:
- bash
- GNU Make
- git
- Docker
- Docker-compose
- Free port 80 (nginx)
- have [hostsupdater](https://github.com/nstapelbroek/hostupdater) running connected to a docker network called `hostsupdater`

For deployment:
- a Gitlab account for the CI/CD pipeline
- a Kubernetes cluster to deploy to
- a (sub-) domain name
